package r3.spring.bean;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import r3.rpc.RpcServer;
import r3.spring.rpc.SpringInvokePolicy;

/**
 * ApplicationBean
 *
 * @author zhoufn
 * @create 2017-12-25 10:22
 **/
public class ApplicationBean implements InitializingBean,ApplicationContextAware{

    @Setter@Getter private String id;

    @Setter@Getter private String name;

    @Setter@Getter private String host;

    @Setter@Getter private int port;

    private RpcServer rpcServer;

    @Getter private static ApplicationContext context;

    /**
     * Invoked by a BeanFactory after it has set all bean properties supplied
     * (and satisfied BeanFactoryAware and ApplicationContextAware).
     * <p>This method allows the bean instance to perform initialization only
     * possible when all bean properties have been set and to throw an
     * exception in the event of misconfiguration.
     *
     * @throws Exception in the event of misconfiguration (such
     *                   as failure to set an essential property) or if initialization fails.
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        rpcServer = new RpcServer(host,port,SpringInvokePolicy.class);
        rpcServer.start();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}
