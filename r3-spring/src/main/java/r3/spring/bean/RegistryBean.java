package r3.spring.bean;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.InitializingBean;
import r3.registry.RegistryCenter;
import r3.zookeeper.ZookeeperRegistryCenter;

/**
 * RegistryBean
 *
 * @author zhoufn
 * @create 2017-12-26 9:36
 **/
public class RegistryBean implements InitializingBean{

    /**
     * zookeeper的地址
     */
    @Getter
    @Setter
    private String address;

    /**
     * 当前应用的命名空间
     */
    @Getter
    @Setter
    private String namespace;

    @Getter
    @Setter
    private int sessionTimeout;

    @Getter
    @Setter
    private int connectionTimeout;

    @Getter
    @Setter
    private int interval;

    /**
     * 注册中心
     */
    @Getter private RegistryCenter registryCenter;


    /**
     * Invoked by a BeanFactory after it has set all bean properties supplied
     * (and satisfied BeanFactoryAware and ApplicationContextAware).
     * <p>This method allows the bean instance to perform initialization only
     * possible when all bean properties have been set and to throw an
     * exception in the event of misconfiguration.
     *
     * @throws Exception in the event of misconfiguration (such
     *                   as failure to set an essential property) or if initialization fails.
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        this.registryCenter = new ZookeeperRegistryCenter(this.address,this.namespace,this.sessionTimeout,this.connectionTimeout,this.interval);
        this.registryCenter.init();
    }
}
