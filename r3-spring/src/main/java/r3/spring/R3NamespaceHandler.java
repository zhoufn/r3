package r3.spring;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;
import r3.spring.bean.ApplicationBean;
import r3.spring.bean.LeaderBean;
import r3.spring.bean.RegistryBean;
import r3.spring.bean.WorkerBean;

/**
 * R3NamespaceHandler
 *
 * @author zhoufn
 * @create 2017-12-25 10:16
 **/
public class R3NamespaceHandler extends NamespaceHandlerSupport {

    /**
     * 实现和Spring的对接
     */
    public void init() {
        registerBeanDefinitionParser("application",new R3BeanDefinitonParser(ApplicationBean.class));
        registerBeanDefinitionParser("registry",new R3BeanDefinitonParser(RegistryBean.class));
        registerBeanDefinitionParser("worker",new R3BeanDefinitonParser(WorkerBean.class));
        registerBeanDefinitionParser("leader",new R3BeanDefinitonParser(LeaderBean.class));
    }
}
