package r3.cluster.router;

import lombok.Getter;
import lombok.Setter;
import r3.common.R3Url;
import r3.rpc.RpcRequest;

/**
 * RouterEntity
 *
 * @author zhoufn
 * @create 2018-01-03 14:38
 **/
public class RouterEntity {

    @Setter@Getter private R3Url url;

    @Setter@Getter private RpcRequest request;

    public RouterEntity() {
    }

    public RouterEntity(R3Url url, RpcRequest request) {
        this.url = url;
        this.request = request;
    }
}
