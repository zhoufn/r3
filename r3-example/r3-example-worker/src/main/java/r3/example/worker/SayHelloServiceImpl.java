package r3.example.worker;

import r3.example.api.SayHelloService;

import java.util.ArrayList;
import java.util.List;

/**
 * SayHelloServiceImpl
 *
 * @author zhoufn
 * @create 2017-12-25 14:19
 **/
public class SayHelloServiceImpl implements SayHelloService {

    @Override
    public List<String> sayHello(List<String> names) {
        List<String> newNames = new ArrayList<>();
        for (String name : names) {
            String newName = "hello " + name;
            newNames.add(newName);
        }
        return newNames;
    }
}
