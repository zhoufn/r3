package r3.example.api;

import r3.example.handler.SayHelloInHandler;
import r3.example.handler.SayHelloOutHandler;
import r3.flow.annotation.In;
import r3.flow.annotation.Out;

import java.util.List;

public interface SayHelloService {

    @Out(SayHelloOutHandler.class)
    @In(SayHelloInHandler.class)
    List<String> sayHello(List<String> names);

}
