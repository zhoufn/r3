package r3.example.handler;

import r3.flow.OutHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * SayHelloOutHandler
 *
 * @author zhoufn
 * @create 2018-01-03 16:42
 **/
public class SayHelloOutHandler extends OutHandler {

    /**
     * 用户自定义分流策略
     *
     * @param parameters 方法参数列表
     * @param shardCount 当前分片数
     * @return
     * @throws Throwable
     */
    @Override
    public List<Object[]> shard(Object[] parameters, int shardCount) throws Throwable {
        List<Object[]> result = new ArrayList<>();
        List<String> param = (List<String>) parameters[0];
        int perSize = param.size() / shardCount;
        for(int i=0;i<shardCount;i++){
            result.add(new Object[]{param.subList(i*perSize,(i!=shardCount-1?(i+1)*perSize:param.size()))});
        }
        return result;
    }
}
